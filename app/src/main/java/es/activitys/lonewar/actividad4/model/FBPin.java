package es.activitys.lonewar.actividad4.model;

/**
 * Created by Pablo on 10/01/2018.
 */

public class FBPin {

    public double lat;
    public double lon;
    public String id;
    public String address;

    public FBPin(){}

    public FBPin(String id, double lat, double lon,String address){
        this.id = id;
        this.lon = lon;
        this.lat = lat;
        this.address = address;
    }

}
