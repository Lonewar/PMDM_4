package es.activitys.lonewar.actividad4.firebase;

import android.util.Log;

import com.google.android.gms.maps.model.LatLng;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import es.activitys.lonewar.actividad4.MapsActivity;
import es.activitys.lonewar.actividad4.model.FBPin;
import es.activitys.lonewar.actividad4.model.PinInfo;

/**
 * Created by Pablo on 12/01/2018.
 */

public class FireBaseAdmin {

    private MapsActivity mapsActivity;
    private DatabaseReference firebaseDatabase;

    public FireBaseAdmin(MapsActivity mapsActivity){
        this.mapsActivity = mapsActivity;
        firebaseDatabase = FirebaseDatabase.getInstance().getReference();
    }

    public void dataUpdate(String branch){

        firebaseDatabase.child(branch).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Log.d("MainActivity","Data change to load pin");
                for(DataSnapshot uniquePinSnapshot:dataSnapshot.getChildren()){
                    FBPin pin = new FBPin((String)uniquePinSnapshot.child("id").getValue(),
                            (double)uniquePinSnapshot.child("lat").getValue(),
                            (double)uniquePinSnapshot.child("lon").getValue(),
                            (String)uniquePinSnapshot.child("address").getValue());
                    mapsActivity.onRetrieveData(pin);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }


        });
    }

    public void addPin(FBPin pin){

        Log.d("MainActivity","Añadiendo pin a firebase con referencia "+firebaseDatabase);
        Log.d("MainActivity", "Id: "+pin.id+" Lon: "+pin.lon+" Lat: "+pin.lat);


        firebaseDatabase.child("pines").child(pin.id).setValue(pin);

    }

    public void updateDevicePosition(LatLng latLng){
        Log.d("MainActivity","Updating device position with "+latLng.latitude+" latitude and "+latLng.longitude+" longitude");
        firebaseDatabase.child("deviceLocation").setValue(latLng);
    }

}
