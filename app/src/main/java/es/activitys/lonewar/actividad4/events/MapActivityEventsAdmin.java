package es.activitys.lonewar.actividad4.events;

import android.location.Location;
import android.location.LocationListener;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;

import es.activitys.lonewar.actividad4.MapsActivity;

import static android.content.ContentValues.TAG;


/**
 * Created by Pablo on 10/01/2018.
 */

public class MapActivityEventsAdmin implements OnCompleteListener {

    private MapsActivity mapsActivity;

    public MapActivityEventsAdmin(MapsActivity mapsActivity){
        this.mapsActivity = mapsActivity;
    }

    @Override
    public void onComplete(@NonNull Task task) {
        if(task.isSuccessful()){
            Log.d(TAG, "onComplete: found location!");
            Location currentLocation = (Location) task.getResult();

            mapsActivity.moveCamera(new LatLng(currentLocation.getLatitude(), currentLocation.getLongitude()),
                    15f,
                    "My Location");
        }else{
            Log.d(TAG, "onComplete: current location is null");
            Toast.makeText(mapsActivity, "unable to get current location", Toast.LENGTH_SHORT).show();
        }
    }

}
