package es.activitys.lonewar.actividad4.firebase;

/**
 * Created by Pablo on 12/01/2018.
 */

import com.google.firebase.database.DataSnapshot;

import es.activitys.lonewar.actividad4.model.FBPin;
import es.activitys.lonewar.actividad4.model.PinInfo;

public interface DatabaseListeners{

    public void onRetrieveData(FBPin pin);
    public void addPin(String id, double lat, double lon, String address);

}
