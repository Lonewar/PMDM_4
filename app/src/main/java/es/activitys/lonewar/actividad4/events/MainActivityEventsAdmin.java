package es.activitys.lonewar.actividad4.events;

import android.content.Intent;
import android.view.View;

import es.activitys.lonewar.actividad4.MainActivity;
import es.activitys.lonewar.actividad4.MapsActivity;
import es.activitys.lonewar.actividad4.R;

/**
 * Created by Pablo on 10/01/2018.
 */

public class MainActivityEventsAdmin implements View.OnClickListener {

    private MainActivity main;


    public MainActivityEventsAdmin(MainActivity main){
        this.main = main;
    }

    @Override
    public void onClick(View v){
        if(v.getId() == R.id.btnMap) {
            Intent intent = new Intent(main, MapsActivity.class);
            main.startActivity(intent);
        }
    }


}
